This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password and stream key**: abc123<br/>

The admin page of OwnCast is located at the $CLOUDRON-APP-ORIGIN/admin .

Please change the stream key (which is your admin password) immediately in `Configuration` -> `Server Setup`.

