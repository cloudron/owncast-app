#!/bin/bash
set -eu

mkdir -p /app/data/data

chown -R cloudron:cloudron /app/data

echo "==> Starting OwnCast"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/owncast -webserverip 0.0.0.0 -webserverport 8080 -rtmpport "${RTMP_PORT:-1935}"

