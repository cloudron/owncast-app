FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=owncast/owncast versioning=semver extractVersion=^v(?<version>.+)$
ARG OWNCAST_VERSION=0.2.1

RUN wget "https://github.com/owncast/owncast/releases/download/v${OWNCAST_VERSION}/owncast-${OWNCAST_VERSION}-linux-64bit.zip" -O /app/code/owncast.zip && \
    unzip /app/code/owncast.zip -d /app/code && \
    rm /app/code/owncast.zip && \
    rm -rf /app/code/data && ln -s /app/data/data /app/code/data && \
    chown -R cloudron:cloudron /app/code

ADD start.sh /app/code/

CMD [ "/app/code/start.sh" ]
