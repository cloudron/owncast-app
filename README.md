# Owncast Cloudron App

This repository contains the Cloudron app package source for Owncast, a self-hosted live video and web chat server for use with existing popular broadcasting software.
Repo: https://github.com/owncast/owncast

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=online.owncast.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id online.owncast.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd owncast-app

cloudron build
cloudron install
```
