## About

Owncast is a self-hosted live video and web chat server for use with existing popular broadcasting software. Point your live stream at a server you personally control and regain ownership over your content.

## Features

* Self hosted and Independent - Have complete control and ownership over your stream, allowing you to create the content and community you want.
* Chat - The frictionless built-in chat allows your viewers to be a part of the action. Include custom emotes and build chat bots to encourage engagement from your viewers.
* Works with your software - Point your existing broadcasting software at your Owncast server and begin streaming.
* External Storage - Owncast can work with different storage providers to optionally scale your videos to many viewers using only low-powered hardware.

